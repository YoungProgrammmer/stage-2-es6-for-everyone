import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {

  let keysPressed = {};
  let firstPlayerCriticalHitTimer = 11
  let secondPlayerCriticalHitTimer = 11

  let firstPlayerTimerStart = setInterval(() => {}, 1000)
  let secondPlayerTimerStart = setInterval(() => {}, 1000)

  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    const myFirstFighter = {
      ...firstFighter,
      healthBar: document.getElementById('left-fighter-indicator'),
    }
    const mySecondFighter = {
      ...secondFighter,
      healthBar: document.getElementById('right-fighter-indicator'),
    }

    document.addEventListener('keydown', (event) => {
      keysPressed[event.code] = true;

      if( event.code === controls.PlayerOneAttack){
        let damage = getDamage(myFirstFighter, mySecondFighter)

        if (keysPressed[controls.PlayerTwoBlock] && event.code == controls.PlayerOneAttack) {
          damage = 0
        }

        getDamageToHealth(myFirstFighter, mySecondFighter, damage, resolve)
      }

      if( event.code === controls.PlayerTwoAttack){
        let damage = getDamage(mySecondFighter, myFirstFighter)

        if (keysPressed[controls.PlayerOneBlock] && event.code == controls.PlayerTwoAttack) {
          damage = 0
        }

        getDamageToHealth(mySecondFighter, myFirstFighter, damage, resolve)
      }

      if (keysPressed[controls.PlayerOneCriticalHitCombination[0]]
        && keysPressed[controls.PlayerOneCriticalHitCombination[1]]
        && keysPressed[controls.PlayerOneCriticalHitCombination[2]]) {
        if (firstPlayerCriticalHitTimer > 10){
          clearInterval(firstPlayerTimerStart)
          firstPlayerCriticalHitTimer = 0
          firstPlayerTimerStart = setInterval(() => {
            firstPlayerCriticalHitTimer++
          }, 1000)
          let criticalDamage = getCriticalDamage(myFirstFighter, mySecondFighter)
          getDamageToHealth(myFirstFighter, mySecondFighter, criticalDamage, resolve)
        }
      }

      if (keysPressed[controls.PlayerTwoCriticalHitCombination[0]]
        && keysPressed[controls.PlayerTwoCriticalHitCombination[1]]
        && keysPressed[controls.PlayerTwoCriticalHitCombination[2]]) {
        if (secondPlayerCriticalHitTimer > 10){
          clearInterval(secondPlayerTimerStart)
          secondPlayerCriticalHitTimer = 0
          secondPlayerTimerStart = setInterval(() => {
            secondPlayerCriticalHitTimer++
          }, 1000)
          let criticalDamage = getCriticalDamage(mySecondFighter, myFirstFighter)
          getDamageToHealth(mySecondFighter, myFirstFighter, criticalDamage, resolve)
        }
      }

    })

    document.addEventListener('keyup', (event) => {
      delete keysPressed[event.code];
    });
  });

}


export function getDamageToHealth(attacker, defender, damage, resolve) {
  const currentHealth = (defender.healthBar.clientWidth / defender.healthBar.parentElement.clientWidth * 100).toFixed();
  const damageToHealth = ((damage / defender.health) * 100).toFixed();
  const changeToHealth = +currentHealth - +damageToHealth;
  if(changeToHealth < 0){
    defender.healthBar.style.width = 0 + 'px';
    resolve(attacker);
  }
  defender.healthBar.style.width = changeToHealth + '%';
}

export function getCriticalDamage(attacker, defender) {
  return getDamage(attacker, defender)*2
}

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
  const attack = fighter.attack;
  const criticalHitChance = Math.random() + 1;
  const power = attack * criticalHitChance;
  return power;
}

export function getBlockPower(fighter) {
  const defense = fighter.defense;
  const dodgeChance = Math.random() + 1;
  const power = defense * dodgeChance;
  return power;
}
