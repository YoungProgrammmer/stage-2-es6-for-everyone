import { createFighterImage } from '../fighterPreview';
import { showModal } from './modal';

export function showWinnerModal(fighter) {
  const bodyElement = createFighterImage(fighter)
  showModal({ title: fighter.name + "Winner!", bodyElement });
}